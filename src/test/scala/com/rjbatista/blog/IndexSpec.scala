package com.rjbatista.blog

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import org.scalatest.WordSpec
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers

import com.rjbatista.blog.Blog.PostMeta
import com.rjbatista.blog.Blog.PostHeader
import com.rjbatista.blog.Index.PostsList
import com.typesafe.config.ConfigFactory

class IndexSpec extends WordSpec with BeforeAndAfterAll with Matchers {
  val testkit = ActorTestKit()
  val config = ConfigFactory.load()
  val path = config.getString("blog.posts.path")

  "The index actor" must {
    "be able to list all posts metadata" in {
      val index = testkit.spawn(Index(), "Index")
      val probe = testkit.createTestProbe[Index.IndexView]()
      index ! Index.ListPosts(probe.ref)
      val postHeader = PostHeader("Test Post", List("Test Author"), List("Test", "tag"), "2020-01-01", "2020-01-02")
      val postMeta = PostMeta(postHeader, s"$path/Test.md")
      probe.expectMessage(PostsList(List(postMeta)))
    }
    "be able to list posts for an author" in {

    }
    "be able to list posts for a tag" in {

    }
    "be able to produce a paginated output" in {

    }
    "be able to find an existing post" in {

    }
  }

  override def afterAll(): Unit = testkit.shutdownTestKit()
}
