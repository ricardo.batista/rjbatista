package com.rjbatista

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.ActorSystem
import com.rjbatista.http.Server
import com.rjbatista.http.Routes

object Main extends App {
  object Guardian {
    def apply(): Behavior[Nothing] = {
      Behaviors.setup[Nothing] { context =>
        Behaviors.empty
      }
    }
  }

  val system = ActorSystem[Nothing](Guardian(), "blog")
  new Server(new Routes(system).routes, system).start()
}
