package com.rjbatista.blog

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import com.rjbatista.blog.Blog.PostMeta
import com.rjbatista.blog.Blog.PostHeader
import akka.actor.typed.ActorRef
import java.io.File
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import scala.io.Source
import upickle.default._

object Index {

  sealed trait IndexCommand
  case class ListPosts(ref: ActorRef[IndexView]) extends IndexCommand

  sealed trait IndexView
  case class PostsList(postsList: List[PostMeta]) extends IndexView

  val config: Config = ConfigFactory.load()

  def postMeta(file: File): PostMeta = {
    val source = Source.fromFile(file)
    val content = try source.getLines().mkString("\n") finally source.close()
    val meta = content.slice(content.indexOf("{{{") + 2, content.indexOf("}}}") + 1)
    val postHeader: PostHeader = read[PostHeader](meta)
    PostMeta(postHeader, file.getAbsolutePath())
  }

  def listPosts(): List[PostMeta] = {
    val dir = new File(config.getString("blog.posts.path"))
    if (dir.exists() && dir.isDirectory()) {
      dir.listFiles().filter(_.isFile()).map(postMeta(_)).toList
    } else {
      List[PostMeta]()
    }
  }

  def apply(): Behavior[IndexCommand] = Behaviors.receive { (context, message) =>
    message match {
      case ListPosts(ref) => {
        ref ! PostsList(listPosts())
      }
    }
    Behaviors.same
  }
}
