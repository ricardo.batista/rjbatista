package com.rjbatista.blog

import upickle.default.{ReadWriter => RW, macroRW}

object Blog {
  case class PostHeader(title: String, author: List[String], tag: List[String], created: String, updated: String)
  object PostHeader {
    implicit val rw: RW[PostHeader] = macroRW
  }

  case class PostMeta(header: PostHeader, path: String)
}
