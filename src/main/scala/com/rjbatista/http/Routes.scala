package com.rjbatista.http

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.ContentTypes
import akka.actor.typed.ActorSystem

class Routes(system: ActorSystem[Nothing]) {
  val routes: Route = pathEndOrSingleSlash {
    val content = view.home.html.index.render()
    complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, content.toString()))
  }
}
